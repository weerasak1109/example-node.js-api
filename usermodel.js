// usermodel.js

var mongoose = require("mongoose");

var UserSchema = mongoose.Schema(
  {
    // กำหนด ชื่อและชนิดของ document เรา
    user_id:{
        type: Number
    },
    username: {
      type: String
    },
    f_name: {
      type: String
    }
  },
  {
    // กำหนด collection ของ MongoDB หรือจะไม่กำหนดก็ได้
    collection: "z_user"
  }
);

// ถ้าไม่ได้กำหนด collection ข้างบน default จะเป็น "foods"
var User = mongoose.model("z_user", UserSchema);
module.exports = User;