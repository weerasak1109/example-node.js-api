// userrouter.js

var express = require("express");
var router = express.Router();
var User = require("./usermodel"); // model


// GET all
router.get("/", (req, res) => {
    User.find().exec((err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send(data);
    });
  });


  module.exports = router;