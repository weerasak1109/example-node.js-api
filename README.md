- API Node.js 
- MongoDB

*** ข้อมูลเอามาจากด้านล่างนี้ ขอบคุณครับ ***
EP 1: https://medium.com/@sariz.wachirasook/restapi-mongo-d5b1ed71c178
EP 2: https://medium.com/@sariz.wachirasook/restapi-mongo-2-47e0ed870981
EP 3: https://medium.com/@sariz.wachirasook/rest-api-mongodb-48b75104750

รวมคำสั่ง
ปล. เปิด cmd เลือกไปที่อยู่ที่ต้องการจะเอาโปรเจคไว้

###สร้างไฟล์โปรเจค จะได้ package.json ขึ้นมาเป็นไฟล์เริ่มต้นของโปรเจ็คเรา
> npm init

###ติดตั้ง express และ cors
> npm install express
> npm install cors
 หรือ 
>npm install express cors

###ติดตั้ง mongoose ไว้สำหรับต่อ mongoDB และ body-parser 
> npm install mongoose
> npm install body-parser
 หรือ 
> npm install mongoose body-parser

###เริ่มทำงานโปรแกรม
>node index.js